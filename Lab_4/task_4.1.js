function addClass(obj, cls) {
    let classes = obj.className.split(' ');

    if (!classes.includes(cls)) {
      classes.push(cls);
    }

    obj.className = classes.join(' ');
  }
  
  let obj = {
    className: 'open menu'
  };

  addClass(obj, 'new');
  addClass(obj, 'open');
  addClass(obj, 'me');
  
  alert(obj.className);
  