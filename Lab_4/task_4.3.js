function Dog(name, breed, age) {
    this.name = name;
    this.breed = breed;
    this.age = age;
    
    this.speak = function() {
      if (this.age < 1) {
        alert('Тяф');
      } else if (this.age >= 1 && this.age < 3) {
        alert('Гав');
      } else {
        alert('Ррр');
      }
    };
  }
  
  const dog1 = new Dog('Бублик', 'Вельш-коргі-пемброк', 0.7);
  const dog2 = new Dog('Тайсон', 'Ротвейлер', 2);
  const dog3 = new Dog('Барон', 'Німецька вівчарка', 8);
  
  dog1.speak();
  dog2.speak();
  dog3.speak();
  