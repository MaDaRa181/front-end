let wallet = prompt("Сума грн у гаманці:");
let burgerPrice = prompt("Ціна одного бургера:");
let burgerNum = Math.floor(wallet / burgerPrice);
let rest = wallet - burgerNum * burgerPrice;
let burgerWord = burgerNum % 10;
let lastWord;
switch (burgerWord) {
  case 1:
    lastWord = "бургер";
    break;
  case 2:
  case 3:
  case 4:
    lastWord = "бургери";
    break;
  default:
    lastWord = "бургерів";
    break;
}
alert(`Ви можете купити ${burgerNum} ${lastWord} і у вас залишиться ${rest} грн решти.`);
