function unique(arr) {
    return arr.filter(function (item, index, array) {
        return array.indexOf(item) === index;
    });
}

let arr = ["Sanji", "Zorro", "Nami", "Zorro", "Luffy", "Nami", "Robin", "Sanji", "Luffy", "Zorro"];
let uniqueArr = unique(arr);
alert(unique(arr));