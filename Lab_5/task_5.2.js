function dispTable(strings) {
    var table = document.createElement("table");

    for (var i = 0; i < strings.length; i++) {
        var row = document.createElement("tr");
        var cell = document.createElement("td");

        cell.textContent = strings[i];

        row.appendChild(cell);
        table.appendChild(row);
    }

    document.body.appendChild(table);
}
