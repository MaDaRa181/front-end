function someFunction(...params) {
    var maxNumber = -Infinity;
    var sum = 0;
    var sentence = '';
  
    for (var i = 0; i < params.length; i++) {
      var param = params[i];
  
      if (typeof param === 'number') {
        if (param > maxNumber) {
          maxNumber = param;
        }
        sum += param;
      } else if (typeof param === 'string') {
        sentence += param + ' ';
      }
    }
  
    if (maxNumber !== -Infinity) {
      console.log('Максимальне число: ' + maxNumber);
    } else {
      console.log('Параметри не включають жодного числа.');
    }
  
    console.log('Сума: ' + sum);
  
    if (sentence !== '') {
      console.log('Речення: ' + sentence.trim());
    } else {
      console.log('Параметри не включають жодного слова.');
    }
  }
  