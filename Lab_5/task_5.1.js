function displTextFontSize(text, fontSize) {
  var element = document.createElement("p");
  element.textContent = text;
  element.style.fontSize = fontSize;
  document.body.appendChild(element);
}
