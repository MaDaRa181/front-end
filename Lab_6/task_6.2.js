function formatDate(inputDate) {
    var currentDate = new Date();
    var dateParts = inputDate.split(/[./-]/);

    var day = parseInt(dateParts[0], 10);
    var month = parseInt(dateParts[1], 10);
    var year = parseInt(dateParts[2], 10);

    var date = new Date(year, month - 1, day);

    if (isSameDay(currentDate, date)) {
        return 'Сьогодні';
    } else if (isYesterday(currentDate, date)) {
        return 'Вчора';
    } else if (isWithinRange(currentDate, date, 2, 6)) {
        var daysAgo = getDaysAgo(currentDate, date);
        return daysAgo + ' ' + getDaysAgoWord(daysAgo) + ' тому';
    } else if (isWithinRange(currentDate, date, 7, 364)) {
        return 'Тиждень тому';
    } else if (isSameYear(currentDate, date)) {
        return 'Рік тому';
    } else {
        return formatDateToString(date);
    }
}

function isSameDay(date1, date2) {
    return date1.toDateString() === date2.toDateString();
}

function isYesterday(date1, date2) {
    var yesterday = new Date(date1);
    yesterday.setDate(yesterday.getDate() - 1);
    return yesterday.toDateString() === date2.toDateString();
}

function isWithinRange(date1, date2, minDays, maxDays) {
    var diff = Math.floor((date1 - date2) / (1000 * 60 * 60 * 24));
    return diff >= minDays && diff <= maxDays;
}

function getDaysAgo(date1, date2) {
    return Math.floor((date1 - date2) / (1000 * 60 * 60 * 24));
}

function getDaysAgoWord(daysAgo) {
    var lastDigit = daysAgo % 10;
    if (lastDigit === 1 && daysAgo !== 11) {
        return 'день';
    } else if (lastDigit >= 2 && lastDigit <= 4 && (daysAgo < 10 || daysAgo > 20)) {
        return 'дні';
    } else {
        return 'днів';
    }
}

function isSameYear(date1, date2) {
    return date1.getFullYear() === date2.getFullYear();
}

function formatDateToString(date) {
    var year = date.getFullYear();
    var month = padZero(date.getMonth() + 1);
    var day = padZero(date.getDate());
    return year + '.' + month + '.' + day;
}

function padZero(number) {
    return number.toString().padStart(2, '0');
}
