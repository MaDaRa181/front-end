function toCamelCase(snakeCase) {
    return snakeCase.replace(/_([a-z])/g, function(match, letter) {
      return letter.toUpperCase();
    });
  }
  