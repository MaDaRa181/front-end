function toSnakeCase(camelCase) {
    return camelCase.replace(/[A-Z]/g, function(match) {
      return '_' + match.toLowerCase();
    });
  }
  