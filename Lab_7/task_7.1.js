function findDates(text) {
    var regex = /\d{4}-\d{2}-\d{2}/g;
    var dates = text.match(regex);
    return dates;
  }
  