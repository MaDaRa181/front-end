function findHexColor(text) {
    var regex = /#([A-Fa-f0-9]{3}|[A-Fa-f0-9]{6})\b/g;
    var colors = text.match(regex);
  
    if (colors !== null) {
      return colors;
    } else {
      return null;
    }
  }
  